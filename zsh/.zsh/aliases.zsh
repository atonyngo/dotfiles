alias ..='cd ..'
alias ...='cd ../..'

alias pingoo='ping google.com'
alias pinga='ping archlinux.org'

alias mkdir='mkdir -p'
alias ls='ls --color --group-directories-first -p'

alias :q='exit'

alias startx='startx > $HOME/.Xlog 2>&1'

alias battery='cat /sys/class/power_supply/BAT0/capacity'

# Cause I'm just an idiot and love typing "yaya"
alias yaya='yay'

alias anki='ANKI_NOHIGHDPI=1 setsid anki --no-sandbox > /dev/null 2>&1 &'
alias qbittorrent='QT_AUTO_SCREEN_SCALE_FACTOR=0 qbittorrent > /dev/null 2>&1 &'

alias paclean='sudo pacman -R $(pacman -Qtdq)'

alias vim='nvim'

function chpwd() {
	emulate -L zsh
	ls --color --group-directories-first -p
}
