export VISUAL=nvim
export EDITOR="$VISUAL"

export CHROME_EXECUTABLE=/usr/bin/chromium

export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx

export GOPATH=$HOME/.go

export QT_QPA_PLATFORM=wayland
export ANKI_WAYLAND=1
