source "$HOME/.antigen/antigen.zsh"

## Antigen stuff
antigen use oh-my-zsh

## Plugins
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting

antigen bundle git

## Theme
antigen bundle subnixr/minimal

antigen apply

## Load configs
for config (~/.zsh/*.zsh) source $config
