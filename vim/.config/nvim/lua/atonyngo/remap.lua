vim.keymap.set("n", "<leader>ft", vim.cmd.Ex)

vim.keymap.set("n", "<leader>p", vim.cmd.Lazy)
vim.keymap.set("n", "<leader>m", vim.cmd.Mason)

vim.keymap.set("v", "I", ":m '<-2<CR>gv=gv")
vim.keymap.set("v", "K", ":m '>+1<CR>gv=gv")

vim.keymap.set("n", "<PageUp>", "<C-u>zz")
vim.keymap.set("n", "<PageDown>", "<C-d>zz")
vim.keymap.set("v", "<PageUp>", "<C-u>zz")
vim.keymap.set("v", "<PageDown>", "<C-d>zz")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "nzzzv")

vim.keymap.set("x", "p", "\"_dP")

vim.keymap.set("n", "Q", "<nop>")

vim.keymap.set("v", "<Tab>", ">gv")
vim.keymap.set("v", "<S-Tab>", "<gv")
