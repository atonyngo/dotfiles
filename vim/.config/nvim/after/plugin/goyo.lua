vim.api.nvim_create_autocmd('User', {
    pattern = 'GoyoEnter',
    command = "Limelight"
})

vim.api.nvim_create_autocmd('User', {
    pattern = 'GoyoLeave',
    command = "Limelight!"
})

vim.keymap.set("n", "<leader>ff", vim.cmd.Goyo)

vim.g.goyo_width = 100

vim.g.limelight_conceal_ctermfg = 'gray'
vim.g.limelight_conceal_guifg = 'gray'
vim.g.limelight_paragraph_span = 1
vim.g.limelight_priority = -1
