vim.keymap.set({"n", "x"}, "<F9>", "<Cmd>DlvToggleBreakpoint<CR>")
vim.keymap.set({"n", "x"}, "<F5>", "<Cmd>DlvDebug<CR>")
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>")
