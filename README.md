## Requirements:

### Common
+ zsh
    + antigen

### X Setup
+ bspwm
+ xorg-xset
+ xorg-xmodmap
+ urxvt
+ input-gestures
    + On laptops
+ ibus

### Wayland Setup
+ niri
+ waybar
+ mako
+ fuzzel
+ alacritty

## How To Install:
`stow` is your friend.
