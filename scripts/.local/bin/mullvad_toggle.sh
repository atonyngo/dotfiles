#!/bin/bash

status=$(mullvad status | awk 'NR==1{print $1}')
if [[ $status = 'Connected' ]]; then
    mullvad disconnect
else
    mullvad connect   
fi

exit 1
