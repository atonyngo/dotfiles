#!/bin/bash

status=$(mullvad status | awk 'NR==1{print $1}')
if [[ $status = 'Connected' ]]; then
    msg=''
    tooltip=$(mullvad status | awk 'NR==1{split($0,a,"in "); print a[2]}')
    class='mullvad-connected'
else
    msg=''
    tooltip='Disconnected'
    class='mullvad-disconnected'
fi

json=$(jq -n --arg m "$msg" --arg t "$tooltip" --arg c "$class" '{ text: $m, "tooltip": $t, class: $c }')
echo $json
