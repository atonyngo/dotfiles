#!/bin/bash

get_lang () {
(fuzzel --dmenu --prompt="$1" --lines=12 | sed 's/^.*\[\(.*\)\].*$/\1/g') << \
LANGUAGES
[zh] - Chinese
[nl] - Dutch
[fr] - French
[de] - German
[el] - Greek
[it] - Italian
[ja] - Japanese
[ko] - Korean
[pt] - Portuguese (Portugal, Brazil)
[es] - Spanish
[sv] - Swedish
[vi] - Vietnamese
LANGUAGES
}

# from=$(get_lang "From: ")
# if [ -z "$from" ]; then
#     exit;
# fi

text=$(echo "" | fuzzel --dmenu --prompt-only="English: ")
if [ -z "$text" ]; then
    exit;
fi

to=$(get_lang "Translate to: ")
if [ -z "$to" ]; then
    exit;
fi

trans -no-ansi 'en':$to $text | fuzzel --dmenu --width=50 --lines=20
